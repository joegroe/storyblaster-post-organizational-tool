# StoryBlaster's Post Organizational Tool
Authors: Joseph Gregory and Ellen Shin

In the summer of 2017, I interned as a Software Engineer and co-created an organizational tool for social media posts.

Our goal was to provide a way to filter and search hundreds of thousands of lines of data with an easy UI.


The program is a Flask based system running on a local host:
    * "http://<host:port>"

### Run The Program
```bash
$ python3 postorgtool.py
```

### Usage
Example:

* Under 'Post Type', select 'Text', and hit 'Search"
* This will organize all the Text posts that are within the csv file provided.
